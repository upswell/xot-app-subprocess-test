#include <QCommandLineParser>
#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>


#include "src/testactions.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_DisableShaderDiskCache);

    QGuiApplication app(argc, argv);
    app.setApplicationVersion(APP_VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription("Upswell Cloud, Subprocess Testing Application");

    QCommandLineOption boolOption(QStringList() << "b" << "bool",
        QCoreApplication::translate("main", "Flag/boolean test argument"));
    QCommandLineOption fullscreenOption(QStringList() << "f" << "fullscreen",
        QCoreApplication::translate("main", "Flag/boolean test argument"));
    QCommandLineOption stringOption(QStringList() << "s" << "string",
        QCoreApplication::translate("main", "String test argument"),
        QCoreApplication::translate("main", "string"), "");

    parser.addHelpOption();
    parser.addOption(boolOption);
    parser.addOption(fullscreenOption);
    parser.addOption(stringOption);
    parser.addVersionOption();
    parser.process(app);

    QQmlApplicationEngine engine;

    qInfo().noquote() << "Loading Test Application with Version: " << APP_VERSION;

    engine.rootContext()->setContextProperty("APP_VERSION", APP_VERSION);
    engine.rootContext()->setContextProperty("TestActions", &TestActions::instance());

    // Env
    engine.rootContext()->setContextProperty("XotHostUri", QString::fromUtf8(qgetenv("XOT_HOST_URI")));
    engine.rootContext()->setContextProperty("XotClusterId", QString::fromUtf8(qgetenv("XOT_CLUSTER_ID")));
    engine.rootContext()->setContextProperty("XotNodeId", QString::fromUtf8(qgetenv("XOT_NODE_ID")));
    engine.rootContext()->setContextProperty("XotNodeAlias", QString::fromUtf8(qgetenv("XOT_NODE_ALIAS")));

    // Args
    engine.rootContext()->setContextProperty("ArgBool", parser.isSet(boolOption));
    engine.rootContext()->setContextProperty("ArgsFullscreen", parser.isSet(fullscreenOption));
    if(parser.isSet(stringOption)) {
        engine.rootContext()->setContextProperty("ArgString", parser.value(stringOption));
    } else {
        engine.rootContext()->setContextProperty("ArgString", "");
    }

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
