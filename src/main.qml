import QtQuick.Window 2.2
import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3



ApplicationWindow {

    id: root
    visible: true
    width: 400
    height: 600
    title: qsTr("Upswell Cloud, Subprocess Testing Application, v" + APP_VERSION)
    flags: Qt.WindowStaysOnTopHint

    Material.theme: Material.Dark
    Material.accent: Material.Purple

    Component.onCompleted: {
        if(ArgsFullscreen) {
            root.showFullScreen();
            root.raise();
        }
        console.log("Application Loaded");
    }

    ListModel {
        id: vars

        property string label: ""
        property string val: "-"

        Component.onCompleted: {
            append({"label": "XOT_HOST_URI", "val": XotHostUri})
            append({"label": "XOT_CLUSTER_ID", "val": XotClusterId})
            append({"label": "XOT_NODE_ID", "val": XotNodeId})
            append({"label": "XOT_NODE_ALIAS", "val": XotNodeAlias})
            append({"label": "Arg (String)", "val": ArgString})
            append({"label": "Arg (Bool)", "val": ArgBool ? "true" : "false"})
        }
    }

// --

    Text {
        id: h1
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 20
        anchors.leftMargin: 20
        font.pointSize: 18
        text: "Upswell Cloud"
        color: "#00BCD4"
    }

    Text {
        id: h2
        anchors.top: h1.bottom
        anchors.left: h1.left
        anchors.topMargin: 10
        anchors.leftMargin: 0
        font.pointSize: 14
        text: "Subprocess Testing Application"
        color: "#00BCD4"
    }

    Text {
        id: h3
        anchors.top: h2.bottom
        anchors.left: h2.left
        anchors.topMargin: 50
        anchors.leftMargin: 0
        font.pointSize: 12
        font.weight: Font.Bold
        text: "Environment Variables & Command Line Arguments"
        color: "#ffffff"
    }

    ListView {
        anchors.top: h3.bottom
        anchors.bottom: btnAbort.top
        anchors.left: h1.left
        anchors.topMargin: 20
        anchors.leftMargin: 0

        model: vars
        delegate: argsDelegate
    }

    Component {
        id: argsDelegate

        Rectangle {
            id: wrapper
            anchors.left: parent.left
            anchors.right: parent.right
            height: labelText.height + 10

            Text {
                id: labelText
                text: label
                font.pointSize: 11
                font.weight: Font.Bold
                color: "#CCCCCC"
                width: 150
            }
            Text {
                id: valText
                anchors.left: labelText.right
                text: val ? val : "-"
                font.pointSize: 11
                color: "#CCCCCC"
                width: 200
            }
        }
    }

    Button {
        id: btnAbort
        anchors.bottom: btnExit.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottomMargin: 10
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        width: parent.width

        text: "Abort"
        onClicked: {
            TestActions.applicationAbort();
        }
    }

    Button {
        id: btnExit
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottomMargin: 20
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        width: parent.width

        text: "Exit"
        onClicked: {
            TestActions.applicationExit();
        }
    }
}
