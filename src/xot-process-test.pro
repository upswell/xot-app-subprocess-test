
QMAKE_TARGET_COMPANY = "Upswell LLC"
QMAKE_TARGET_PRODUCT = "xot-process-test"
QMAKE_TARGET_DESCRIPTION = "Upswell Cloud, Subprocess Testing Application"
QMAKE_TARGET_COPYRIGHT = "GNU LGPL v3"


# -- Project Configuration
# ----------------------------------------------------------------------

QT += quick quickcontrols2
CONFIG += c++11

DESTDIR = ../dist

TARGET = xot-process-test

QML_IMPORT_PATH =
QML_DESIGNER_IMPORT_PATH =


# -- Defines
# ----------------------------------------------------------------------

CONFIG(release, debug|release) {
    win32:VERSION = 0.1.5.0
    else:VERSION = 0.1.5
}
CONFIG(debug, debug|release) {
    win32:VERSION = 0.0.0.0
    else:VERSION = dev
}

DEFINES += APP_NS=\\\"io.upswell.xot.test-app\\\"
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


# -- Sources
# ----------------------------------------------------------------------

HEADERS += \
    src\testactions.h

SOURCES += \
    main.cpp \
    src\testactions.cpp

RESOURCES += qml.qrc
