#ifndef TESTACTIONS_H
#define TESTACTIONS_H

#include <QObject>

class TestActions : public QObject
{
    Q_OBJECT
public:

    static TestActions& instance() {
        static TestActions instance;
        return instance;
    }

    Q_INVOKABLE static void applicationAbort();
    Q_INVOKABLE static void applicationExit();

private:

    TestActions(){}

};

#endif // TESTACTIONS_H
