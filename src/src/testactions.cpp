#include <QGuiApplication>

#include "testactions.h"

void TestActions::applicationAbort()
{
    abort();
}

void TestActions::applicationExit()
{
    qApp->exit();
}
