# Upswell Cloud, Subprocess Testing Application

The Subprocess Testing Application provides simple application for use in testing the Upswell Cloud Experience Agent's subprocess management functions such-as environment variable passing and restart on crash.

It is part of the [Upswell Cloud](https://upswell.cloud) Common Application repository providing simple reusable tools for deploying on-site media experiences.

## Dependencies

The following tools are required for development. For installation instructions follow the links below:

* [QT 5.12+](https://www.qt.io/download)


## Quick Start

Open `src/xot-process-test.pro` and select `Run`.


## Configuration

To assist in debugging the Experience Agent select Environment Variables are loaded and displayed directly in the application.

These variables are provided to the application by the Experience Agent by default, but they may be added to your development for testing purposes.

| Variable          | Description           |
| --                | --                    |
| `XOT_HOST_URL`    | The root URI for the Upswell Cloud tenant providing the application.  |
| `XOT_CLUSTER_ID`  | The ID of the Cluster the Experience Agent is currently assigned to.  |
| `XOT_NODE_ID`     | The ID of the Node (computer) the Experience Agent is running on.     |
| `XOT_NODE_ALIAS`  | The Node Alias (common name) for the Node.                            |


Additionally the following Command Line Arguments are available for testing the launch features:

| Argument          | Alias     | Description |
| --                | --        | -- |
| `--bool`          | `-b`      | Argument used to test setting an application flag. |
| `--fullscreen`    | `-f`      | Flag used to launch the application in fullscreen. |
| `--string`        | `-s`      | Argument used to test passing a string to the application.


## About Upswell

[Upswell](https://hello-upswell.com) is an independent creative partner to forward-thinking leaders tackling the complex challenges facing our world today.

We combine storytelling with creative applications of technology to create experiences that stimulate different wats of seeing, understanding, and engaging with the urgent topics of our time.


## About the Upswell Cloud

The [Upswell Cloud](https://upswell.cloud) provides organizations with the tools needed to manage the day-to-day development, deployment and operation of physical media spaces.

For more information contact Upswell's Director of Technology, Kieran Lynn.
